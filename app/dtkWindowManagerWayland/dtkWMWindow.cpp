// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkWMCompositor.h"
#include "dtkWMWindow.h"

#include <QtWaylandCompositor>

dtkWMWindow::dtkWMWindow(void)
{

}

void dtkWMWindow::setCompositor(dtkWMCompositor *comp)
{
    m_compositor = comp;

    connect(m_compositor, &dtkWMCompositor::startMove,   this, &dtkWMWindow::startMove);
    connect(m_compositor, &dtkWMCompositor::startResize, this, &dtkWMWindow::startResize);
    connect(m_compositor, &dtkWMCompositor::dragStarted, this, &dtkWMWindow::startDrag);
}

void dtkWMWindow::initializeGL(void)
{
    QImage backgroundImage = QImage(QLatin1String(":/background.jpg")).rgbSwapped();
    backgroundImage.invertPixels();
    m_backgroundTexture = new QOpenGLTexture(backgroundImage, QOpenGLTexture::DontGenerateMipMaps);
    m_backgroundTexture->setMinificationFilter(QOpenGLTexture::Nearest);
    m_backgroundImageSize = backgroundImage.size();
    m_textureBlitter.create();
    m_compositor->create(); // the compositor's hardware integration may depend on GL
}

void dtkWMWindow::drawBackground(void)
{
    for (int y = 0; y < height(); y += m_backgroundImageSize.height()) {
        for (int x = 0; x < width(); x += m_backgroundImageSize.width()) {
            QMatrix4x4 targetTransform = QOpenGLTextureBlitter::targetTransform(QRect(QPoint(x,y), m_backgroundImageSize), QRect(QPoint(0,0), size()));
            m_textureBlitter.blit(m_backgroundTexture->textureId(), targetTransform, QOpenGLTextureBlitter::OriginTopLeft);
        }
    }
}

QPointF dtkWMWindow::getAnchorPosition(const QPointF &position, int resizeEdge, const QSize &windowSize)
{
    float y = position.y();

    if (resizeEdge & QWaylandXdgSurfaceV5::ResizeEdge::TopEdge)
        y += windowSize.height();

    float x = position.x();

    if (resizeEdge & QWaylandXdgSurfaceV5::ResizeEdge::LeftEdge)
        x += windowSize.width();

    return QPointF(x, y);
}

QPointF dtkWMWindow::getAnchoredPosition(const QPointF &anchorPosition, int resizeEdge, const QSize &windowSize)
{
    return anchorPosition - getAnchorPosition(QPointF(), resizeEdge, windowSize);
}

void dtkWMWindow::paintGL(void)
{
    m_compositor->startRender();

    QOpenGLFunctions *functions = context()->functions();
    functions->glClearColor(1.f, .6f, .0f, 0.5f);
    functions->glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    m_textureBlitter.bind();

    this->drawBackground();

    functions->glEnable(GL_BLEND);
    functions->glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    GLenum currentTarget = GL_TEXTURE_2D;

    foreach(dtkWMView *view, m_compositor->views()) {

        if (view->isCursor())
            continue;

        auto texture = view->getTexture();

        if (!texture)
            continue;

        if (texture->target() != currentTarget) {
            currentTarget = texture->target();
            m_textureBlitter.bind(currentTarget);
        }

        QWaylandSurface *surface = view->surface();

        if ((surface && surface->hasContent()) || view->isBufferLocked()) {

            QSize s = view->size();

            if (!s.isEmpty()) {

                if (m_mousedtkWMView == view && m_grabState == ResizeGrab && m_resizeAnchored)
                    view->setPosition(getAnchoredPosition(m_resizeAnchorPosition, m_resizeEdge, s));

                QPointF pos = view->position() + view->parentPosition();
                QRectF surfaceGeometry(pos, s);
                auto surfaceOrigin = view->textureOrigin();
                auto sf = view->animationFactor();
                QRectF targetRect(surfaceGeometry.topLeft() * sf, surfaceGeometry.size() * sf);
                QMatrix4x4 targetTransform = QOpenGLTextureBlitter::targetTransform(targetRect, QRect(QPoint(), size()));
                m_textureBlitter.blit(texture->textureId(), targetTransform, surfaceOrigin);
            }
        }
    }

    functions->glDisable(GL_BLEND);

    m_textureBlitter.release();
    m_compositor->endRender();
}

dtkWMView *dtkWMWindow::viewAt(const QPointF &point)
{
    dtkWMView *ret = nullptr;

    foreach(dtkWMView *view, m_compositor->views()) {

        if (view == m_dragIcondtkWMView)
            continue;

        QRectF geom(view->position(), view->size());

        if (geom.contains(point))
            ret = view;
    }

    return ret;
}

void dtkWMWindow::startMove(void)
{
    m_grabState = MoveGrab;
}

void dtkWMWindow::startResize(int edge, bool anchored)
{
    m_initialSize = m_mousedtkWMView->windowSize();
    m_grabState = ResizeGrab;
    m_resizeEdge = edge;
    m_resizeAnchored = anchored;
    m_resizeAnchorPosition = getAnchorPosition(m_mousedtkWMView->position(), edge, m_mousedtkWMView->surface()->size());
}

void dtkWMWindow::startDrag(dtkWMView *dragIcon)
{
    m_grabState = DragGrab;
    m_dragIcondtkWMView = dragIcon;
    m_compositor->raise(dragIcon);
}

void dtkWMWindow::mousePressEvent(QMouseEvent *e)
{
    if (mouseGrab())
        return;

    if (m_mousedtkWMView.isNull()) {
        m_mousedtkWMView = viewAt(e->localPos());

        if (!m_mousedtkWMView) {
            m_compositor->closePopups();
            return;
        }

        if (e->modifiers() == Qt::AltModifier || e->modifiers() == Qt::MetaModifier)
            m_grabState = MoveGrab; //start move
        else
            m_compositor->raise(m_mousedtkWMView);

        m_initialMousePos = e->localPos();
        m_mouseOffset = e->localPos() - m_mousedtkWMView->position();

        QMouseEvent moveEvent(QEvent::MouseMove, e->localPos(), e->globalPos(), Qt::NoButton, Qt::NoButton, e->modifiers());
        sendMouseEvent(&moveEvent, m_mousedtkWMView);
    }

    sendMouseEvent(e, m_mousedtkWMView);
}

void dtkWMWindow::mouseReleaseEvent(QMouseEvent *e)
{
    if (!mouseGrab())
        sendMouseEvent(e, m_mousedtkWMView);

    if (e->buttons() == Qt::NoButton) {

        if (m_grabState == DragGrab) {
            dtkWMView *view = viewAt(e->localPos());
            m_compositor->handleDrag(view, e);
        }

        m_mousedtkWMView = nullptr;
        m_grabState = NoGrab;
    }
}

void dtkWMWindow::mouseMoveEvent(QMouseEvent *e)
{
    switch (m_grabState) {
    case NoGrab: {
        dtkWMView *view = m_mousedtkWMView ? m_mousedtkWMView.data() : viewAt(e->localPos());
        sendMouseEvent(e, view);
        if (!view)
            setCursor(Qt::ArrowCursor);
    }; break;
    case MoveGrab: {
        m_mousedtkWMView->setPosition(e->localPos() - m_mouseOffset);
        update();
    }; break;
    case ResizeGrab: {
        QPoint delta = (e->localPos() - m_initialMousePos).toPoint();
        m_compositor->handleResize(m_mousedtkWMView, m_initialSize, delta, m_resizeEdge);
    }; break;
    case DragGrab: {
        dtkWMView *view = viewAt(e->localPos());
        m_compositor->handleDrag(view, e);

        if (m_dragIcondtkWMView) {
            m_dragIcondtkWMView->setPosition(e->localPos() + m_dragIcondtkWMView->offset());
            update();
        }
    }; break;
    }
}

void dtkWMWindow::sendMouseEvent(QMouseEvent *e, dtkWMView *target)
{
    QPointF mappedPos = e->localPos();

    if (target)
        mappedPos -= target->position();

    QMouseEvent viewEvent(e->type(), mappedPos, e->localPos(), e->button(), e->buttons(), e->modifiers());

    m_compositor->handleMouseEvent(target, &viewEvent);
}

void dtkWMWindow::keyPressEvent(QKeyEvent *e)
{
    m_compositor->defaultSeat()->sendKeyPressEvent(e->nativeScanCode());
}

void dtkWMWindow::keyReleaseEvent(QKeyEvent *e)
{
    m_compositor->defaultSeat()->sendKeyReleaseEvent(e->nativeScanCode());
}

//
// dtkWMdtkWMWindow.cpp ends here
