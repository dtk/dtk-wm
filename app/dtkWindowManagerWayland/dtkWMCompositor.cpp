// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkWMCompositor.h"

#ifndef GL_TEXTURE_EXTERNAL_OES
#define GL_TEXTURE_EXTERNAL_OES 0x8D65
#endif

dtkWMView::dtkWMView(dtkWMCompositor *compositor)
    : m_compositor(compositor)
{}

QOpenGLTexture *dtkWMView::getTexture()
{
    bool newContent = advance();
    QWaylandBufferRef buf = currentBuffer();
    if (!buf.hasContent())
        m_texture = nullptr;
    if (newContent) {
        m_texture = buf.toOpenGLTexture();
        if (surface()) {
            m_size = surface()->size();
            m_origin = buf.origin() == QWaylandSurface::OriginTopLeft
                    ? QOpenGLTextureBlitter::OriginTopLeft
                    : QOpenGLTextureBlitter::OriginBottomLeft;
        }
    }

    return m_texture;
}

QOpenGLTextureBlitter::Origin dtkWMView::textureOrigin() const
{
    return m_origin;
}

QSize dtkWMView::size() const
{
    return surface() ? surface()->size() : m_size;
}

bool dtkWMView::isCursor() const
{
    return surface() && surface()->isCursorSurface();
}


void dtkWMView::onXdgSetMaximized()
{
    m_xdgSurface->sendMaximized(output()->geometry().size());

    // An improvement here, would have been to wait for the commit after the ack_configure for the
    // request above before moving the window. This would have prevented the window from being
    // moved until the contents of the window had actually updated. This improvement is left as an
    // exercise for the reader.
    setPosition(QPoint(0, 0));
}

void dtkWMView::onXdgUnsetMaximized()
{
    m_xdgSurface->sendUnmaximized();
}

void dtkWMView::onXdgSetFullscreen(QWaylandOutput* clientPreferredOutput)
{
    QWaylandOutput *outputToFullscreen = clientPreferredOutput
            ? clientPreferredOutput
            : output();

    m_xdgSurface->sendFullscreen(outputToFullscreen->geometry().size());

    // An improvement here, would have been to wait for the commit after the ack_configure for the
    // request above before moving the window. This would have prevented the window from being
    // moved until the contents of the window had actually updated. This improvement is left as an
    // exercise for the reader.
    setPosition(outputToFullscreen->position());
}

void dtkWMView::onOffsetForNextFrame(const QPoint &offset)
{
    m_offset = offset;
    setPosition(position() + offset);
}

void dtkWMView::timerEvent(QTimerEvent *event)
{
    if (event->timerId() != m_animationTimer.timerId())
        return;

    m_compositor->triggerRender();

    if (m_animationCountUp) {
        m_animationFactor += .08;
        if (m_animationFactor > 1.0) {
            m_animationFactor = 1.0;
            m_animationTimer.stop();
            emit animationDone();
        }
    } else {
        m_animationFactor -= .08;
        if (m_animationFactor < 0.01) {
            m_animationFactor = 0.01;
            m_animationTimer.stop();
            emit animationDone();
        }
    }
}

void dtkWMView::startAnimation(bool countUp)
{
    m_animationCountUp = countUp;
    m_animationFactor = countUp ? .1 : 1.0;
    m_animationTimer.start(20, this);
}

void dtkWMView::cancelAnimation()
{
    m_animationFactor = 1.0;
    m_animationTimer.stop();
}

void dtkWMView::onXdgUnsetFullscreen()
{
    onXdgUnsetMaximized();
}

dtkWMCompositor::dtkWMCompositor(QWindow *window)
    : m_window(window)
    , m_wlShell(new QWaylandWlShell(this))
    , m_xdgShell(new QWaylandXdgShellV5(this))
{
    connect(m_wlShell, &QWaylandWlShell::wlShellSurfaceCreated, this, &dtkWMCompositor::onWlShellSurfaceCreated);
    connect(m_xdgShell, &QWaylandXdgShellV5::xdgSurfaceCreated, this, &dtkWMCompositor::onXdgSurfaceCreated);
    connect(m_xdgShell, &QWaylandXdgShellV5::xdgPopupRequested, this, &dtkWMCompositor::onXdgPopupRequested);
}

dtkWMCompositor::~dtkWMCompositor()
{
}

void dtkWMCompositor::create()
{
    QWaylandOutput *output = new QWaylandOutput(this, m_window);
    QWaylandOutputMode mode(QSize(800, 600), 60000);
    output->addMode(mode, true);
    QWaylanddtkWMCompositor::create();
    output->setCurrentMode(mode);

    connect(this, &QWaylanddtkWMCompositor::surfaceCreated, this, &dtkWMCompositor::onSurfaceCreated);
    connect(defaultSeat(), &QWaylandSeat::cursorSurfaceRequest, this, &dtkWMCompositor::adjustCursorSurface);
    connect(defaultSeat()->drag(), &QWaylandDrag::dragStarted, this, &dtkWMCompositor::startDrag);

    connect(this, &QWaylanddtkWMCompositor::subsurfaceChanged, this, &dtkWMCompositor::onSubsurfaceChanged);
}

void dtkWMCompositor::onSurfaceCreated(QWaylandSurface *surface)
{
    connect(surface, &QWaylandSurface::surfaceDestroyed, this, &dtkWMCompositor::surfaceDestroyed);
    connect(surface, &QWaylandSurface::hasContentChanged, this, &dtkWMCompositor::surfaceHasContentChanged);
    connect(surface, &QWaylandSurface::redraw, this, &dtkWMCompositor::triggerRender);

    connect(surface, &QWaylandSurface::subsurfacePositionChanged, this, &dtkWMCompositor::onSubsurfacePositionChanged);
    dtkWMView *view = new dtkWMView(this);
    view->setSurface(surface);
    view->setOutput(outputFor(m_window));
    m_views << view;
    connect(view, &QWaylanddtkWMView::surfaceDestroyed, this, &dtkWMCompositor::viewSurfaceDestroyed);
    connect(surface, &QWaylandSurface::offsetForNextFrame, view, &dtkWMView::onOffsetForNextFrame);
}

void dtkWMCompositor::surfaceHasContentChanged()
{
    QWaylandSurface *surface = qobject_cast<QWaylandSurface *>(sender());
    if (surface->hasContent()) {
        if (surface->role() == QWaylandWlShellSurface::role()
                || surface->role() == QWaylandXdgSurfaceV5::role()
                || surface->role() == QWaylandXdgPopupV5::role()) {
            defaultSeat()->setKeyboardFocus(surface);
        }
    }
    triggerRender();
}

void dtkWMCompositor::surfaceDestroyed()
{
    triggerRender();
}

void dtkWMCompositor::viewSurfaceDestroyed()
{
    dtkWMView *view = qobject_cast<dtkWMView*>(sender());
    view->setBufferLocked(true);
    view->startAnimation(false);
    connect(view, &dtkWMView::animationDone, this, &dtkWMCompositor::viewAnimationDone);
}


void dtkWMCompositor::viewAnimationDone()
{
    dtkWMView *view = qobject_cast<dtkWMView*>(sender());
    m_views.removeAll(view);
    delete view;
}


dtkWMView * dtkWMCompositor::finddtkWMView(const QWaylandSurface *s) const
{
    Q_FOREACH (dtkWMView* view, m_views) {
        if (view->surface() == s)
            return view;
    }
    return nullptr;
}

void dtkWMCompositor::onWlShellSurfaceCreated(QWaylandWlShellSurface *wlShellSurface)
{
    connect(wlShellSurface, &QWaylandWlShellSurface::startMove, this, &dtkWMCompositor::onStartMove);
    connect(wlShellSurface, &QWaylandWlShellSurface::startResize, this, &dtkWMCompositor::onWlStartResize);
    connect(wlShellSurface, &QWaylandWlShellSurface::setTransient, this, &dtkWMCompositor::onSetTransient);
    connect(wlShellSurface, &QWaylandWlShellSurface::setPopup, this, &dtkWMCompositor::onSetPopup);

    dtkWMView *view = finddtkWMView(wlShellSurface->surface());
    Q_ASSERT(view);
    view->m_wlShellSurface = wlShellSurface;
    view->startAnimation(true);
}

void dtkWMCompositor::onXdgSurfaceCreated(QWaylandXdgSurfaceV5 *xdgSurface)
{
    connect(xdgSurface, &QWaylandXdgSurfaceV5::startMove, this, &dtkWMCompositor::onStartMove);
    connect(xdgSurface, &QWaylandXdgSurfaceV5::startResize, this, &dtkWMCompositor::onXdgStartResize);

    dtkWMView *view = finddtkWMView(xdgSurface->surface());
    Q_ASSERT(view);
    view->m_xdgSurface = xdgSurface;

    connect(xdgSurface, &QWaylandXdgSurfaceV5::setMaximized, view, &dtkWMView::onXdgSetMaximized);
    connect(xdgSurface, &QWaylandXdgSurfaceV5::setFullscreen, view, &dtkWMView::onXdgSetFullscreen);
    connect(xdgSurface, &QWaylandXdgSurfaceV5::unsetMaximized, view, &dtkWMView::onXdgUnsetMaximized);
    connect(xdgSurface, &QWaylandXdgSurfaceV5::unsetFullscreen, view, &dtkWMView::onXdgUnsetFullscreen);
    view->startAnimation(true);
}

void dtkWMCompositor::onXdgPopupRequested(QWaylandSurface *surface, QWaylandSurface *parent,
                                     QWaylandSeat *seat, const QPoint &position,
                                     const QWaylandResource &resource)
{
    Q_UNUSED(seat);

    QWaylandXdgPopupV5 *xdgPopup = new QWaylandXdgPopupV5(m_xdgShell, surface, parent, position, resource);

    dtkWMView *view = finddtkWMView(surface);
    Q_ASSERT(view);

    dtkWMView *parentdtkWMView = finddtkWMView(parent);
    Q_ASSERT(parentdtkWMView);

    view->setPosition(parentdtkWMView->position() + position);
    view->m_xdgPopup = xdgPopup;
}

void dtkWMCompositor::onStartMove()
{
    closePopups();
    emit startMove();
}

void dtkWMCompositor::onWlStartResize(QWaylandSeat *, QWaylandWlShellSurface::ResizeEdge edges)
{
    closePopups();
    emit startResize(int(edges), false);
}

void dtkWMCompositor::onXdgStartResize(QWaylandSeat *seat,
                                  QWaylandXdgSurfaceV5::ResizeEdge edges)
{
    Q_UNUSED(seat);
    emit startResize(int(edges), true);
}

void dtkWMCompositor::onSetTransient(QWaylandSurface *parent, const QPoint &relativeToParent, bool inactive)
{
    Q_UNUSED(inactive);

    QWaylandWlShellSurface *wlShellSurface = qobject_cast<QWaylandWlShellSurface*>(sender());
    dtkWMView *view = finddtkWMView(wlShellSurface->surface());

    if (view) {
        raise(view);
        dtkWMView *parentdtkWMView = finddtkWMView(parent);
        if (parentdtkWMView)
            view->setPosition(parentdtkWMView->position() + relativeToParent);
    }
}

void dtkWMCompositor::onSetPopup(QWaylandSeat *seat, QWaylandSurface *parent, const QPoint &relativeToParent)
{
    Q_UNUSED(seat);
    QWaylandWlShellSurface *surface = qobject_cast<QWaylandWlShellSurface*>(sender());
    dtkWMView *view = finddtkWMView(surface->surface());
    if (view) {
        raise(view);
        dtkWMView *parentdtkWMView = finddtkWMView(parent);
        if (parentdtkWMView)
            view->setPosition(parentdtkWMView->position() + relativeToParent);
        view->cancelAnimation();
    }
}

void dtkWMCompositor::onSubsurfaceChanged(QWaylandSurface *child, QWaylandSurface *parent)
{
    dtkWMView *view = finddtkWMView(child);
    dtkWMView *parentdtkWMView = finddtkWMView(parent);
    view->setParentdtkWMView(parentdtkWMView);
}

void dtkWMCompositor::onSubsurfacePositionChanged(const QPoint &position)
{
    QWaylandSurface *surface = qobject_cast<QWaylandSurface*>(sender());
    if (!surface)
        return;
    dtkWMView *view = finddtkWMView(surface);
    view->setPosition(position);
    triggerRender();
}

void dtkWMCompositor::triggerRender()
{
    m_window->requestUpdate();
}

void dtkWMCompositor::startRender()
{
    QWaylandOutput *out = defaultOutput();
    if (out)
        out->frameStarted();
}

void dtkWMCompositor::endRender()
{
    QWaylandOutput *out = defaultOutput();
    if (out)
        out->sendFrameCallbacks();
}

void dtkWMCompositor::updateCursor()
{
    m_cursordtkWMView.advance();
    QImage image = m_cursordtkWMView.currentBuffer().image();
    if (!image.isNull())
        m_window->setCursor(QCursor(QPixmap::fromImage(image), m_cursorHotspotX, m_cursorHotspotY));
}

void dtkWMCompositor::adjustCursorSurface(QWaylandSurface *surface, int hotspotX, int hotspotY)
{
    if ((m_cursordtkWMView.surface() != surface)) {
        if (m_cursordtkWMView.surface())
            disconnect(m_cursordtkWMView.surface(), &QWaylandSurface::redraw, this, &dtkWMCompositor::updateCursor);
        if (surface)
            connect(surface, &QWaylandSurface::redraw, this, &dtkWMCompositor::updateCursor);
    }

    m_cursordtkWMView.setSurface(surface);
    m_cursorHotspotX = hotspotX;
    m_cursorHotspotY = hotspotY;

    if (surface && surface->hasContent())
        updateCursor();
}

void dtkWMCompositor::closePopups()
{
    m_wlShell->closeAllPopups();
    m_xdgShell->closeAllPopups();
}

void dtkWMCompositor::handleMouseEvent(QWaylanddtkWMView *target, QMouseEvent *me)
{
    auto popClient = popupClient();
    if (target && me->type() == QEvent::MouseButtonPress
            && popClient && popClient != target->surface()->client()) {
        closePopups();
    }

    QWaylandSeat *seat = defaultSeat();
    QWaylandSurface *surface = target ? target->surface() : nullptr;
    switch (me->type()) {
        case QEvent::MouseButtonPress:
            seat->sendMousePressEvent(me->button());
            if (surface != seat->keyboardFocus()) {
                if (surface == nullptr
                        || surface->role() == QWaylandWlShellSurface::role()
                        || surface->role() == QWaylandXdgSurfaceV5::role()
                        || surface->role() == QWaylandXdgPopupV5::role()) {
                    seat->setKeyboardFocus(surface);
                }
            }
            break;
    case QEvent::MouseButtonRelease:
         seat->sendMouseReleaseEvent(me->button());
         break;
    case QEvent::MouseMove:
        seat->sendMouseMoveEvent(target, me->localPos(), me->globalPos());
    default:
        break;
    }
}

void dtkWMCompositor::handleResize(dtkWMView *target, const QSize &initialSize, const QPoint &delta, int edge)
{
    QWaylandWlShellSurface *wlShellSurface = target->m_wlShellSurface;
    if (wlShellSurface) {
        QWaylandWlShellSurface::ResizeEdge edges = QWaylandWlShellSurface::ResizeEdge(edge);
        QSize newSize = wlShellSurface->sizeForResize(initialSize, delta, edges);
        wlShellSurface->sendConfigure(newSize, edges);
    }

    QWaylandXdgSurfaceV5 *xdgSurface = target->m_xdgSurface;
    if (xdgSurface) {
        QWaylandXdgSurfaceV5::ResizeEdge edges = static_cast<QWaylandXdgSurfaceV5::ResizeEdge>(edge);
        QSize newSize = xdgSurface->sizeForResize(initialSize, delta, edges);
        xdgSurface->sendResizing(newSize);
    }
}

void dtkWMCompositor::startDrag()
{
    QWaylandDrag *currentDrag = defaultSeat()->drag();
    Q_ASSERT(currentDrag);
    dtkWMView *icondtkWMView = finddtkWMView(currentDrag->icon());
    icondtkWMView->setPosition(m_window->mapFromGlobal(QCursor::pos()));

    emit dragStarted(icondtkWMView);
}

void dtkWMCompositor::handleDrag(dtkWMView *target, QMouseEvent *me)
{
    QPointF pos = me->localPos();
    QWaylandSurface *surface = nullptr;
    if (target) {
        pos -= target->position();
        surface = target->surface();
    }
    QWaylandDrag *currentDrag = defaultSeat()->drag();
    currentDrag->dragMove(surface, pos);
    if (me->buttons() == Qt::NoButton) {
        m_views.removeOne(finddtkWMView(currentDrag->icon()));
        currentDrag->drop();
    }
}

QWaylandClient *dtkWMCompositor::popupClient() const
{
    auto client = m_wlShell->popupClient();
    return client ? client : m_xdgShell->popupClient();
}

// We only have a flat list of views, plus pointers from child to parent,
// so maintaining a stacking order gets a bit complex. A better data
// structure is left as an exercise for the reader.

static int findEndOfChildTree(const QList<dtkWMView*> &list, int index)
{
    int n = list.count();
    dtkWMView *parent = list.at(index);
    while (index + 1 < n) {
        if (list.at(index+1)->parentdtkWMView() != parent)
            break;
        index = findEndOfChildTree(list, index + 1);
    }
    return index;
}

void dtkWMCompositor::raise(dtkWMView *view)
{
    int startPos = m_views.indexOf(view);
    int endPos = findEndOfChildTree(m_views, startPos);

    int n = m_views.count();
    int tail =  n - endPos - 1;

    //bubble sort: move the child tree to the end of the list
    for (int i = 0; i < tail; i++) {
        int source = endPos + 1 + i;
        int dest = startPos + i;
        for (int j = source; j > dest; j--)
            m_views.swap(j, j-1);
    }
}

//
// dtkWMCompositor.cpp ends here
