// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <QtCore>
#include <QtGui>
#include <QtWaylandCompositor>

class dtkWMCompositor;

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class dtkWMView : public QWaylandView
{
    Q_OBJECT

public:
    dtkWMView(dtkWMCompositor *compositor);
    QOpenGLTexture *getTexture();
    QOpenGLTextureBlitter::Origin textureOrigin() const;
    QPointF position() const { return m_position; }
    void setPosition(const QPointF &pos) { m_position = pos; }
    QSize size() const;
    bool isCursor() const;
    bool hasShell() const { return m_wlShellSurface; }
    void setParentdtkWMView(dtkWMView *parent) { m_parentdtkWMView = parent; }
    dtkWMView *parentdtkWMView() const { return m_parentdtkWMView; }
    QPointF parentPosition() const { return m_parentdtkWMView ? (m_parentdtkWMView->position() + m_parentdtkWMView->parentPosition()) : QPointF(); }
    QSize windowSize() { return m_xdgSurface ? m_xdgSurface->windowGeometry().size() :  surface() ? surface()->size() : m_size; }
    QPoint offset() const { return m_offset; }

    qreal animationFactor() const {return m_animationFactor; }
    void setAnimationFactor(qreal f) {m_animationFactor = f; }

signals:
    void animationDone();

protected:
    void timerEvent(QTimerEvent *event) override;

private:
    friend class dtkWMCompositor;
    dtkWMCompositor *m_compositor = nullptr;
    GLenum m_textureTarget = GL_TEXTURE_2D;
    QOpenGLTexture *m_texture = nullptr;
    QOpenGLTextureBlitter::Origin m_origin;
    QPointF m_position;
    QSize m_size;
    QWaylandWlShellSurface *m_wlShellSurface = nullptr;
    QWaylandXdgSurfaceV5 *m_xdgSurface = nullptr;
    QWaylandXdgPopupV5 *m_xdgPopup = nullptr;
    dtkWMView *m_parentdtkWMView = nullptr;
    QPoint m_offset;
    qreal m_animationFactor = 1.0;
    QBasicTimer m_animationTimer;
    bool m_animationCountUp;

public slots:
    void onXdgSetMaximized();
    void onXdgUnsetMaximized();
    void onXdgSetFullscreen(QWaylandOutput *output);
    void onXdgUnsetFullscreen();
    void onOffsetForNextFrame(const QPoint &offset);

    void startAnimation(bool countUp);
    void cancelAnimation();
};

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class dtkWMCompositor : public QWaylanddtkWMCompositor
{
    Q_OBJECT

public:
    dtkWMCompositor(QWindow *window);
    ~dtkWMCompositor() override;

    void create() override;

    void startRender();
    void endRender();

    QList<dtkWMView*> views() const { return m_views; }

    void raise(dtkWMView *view);

    void handleMouseEvent(QWaylanddtkWMView *target, QMouseEvent *me);
    void handleResize(dtkWMView *target, const QSize &initialSize, const QPoint &delta, int edge);
    void handleDrag(dtkWMView *target, QMouseEvent *me);

    QWaylandClient *popupClient() const;
    void closePopups();

protected:
    void adjustCursorSurface(QWaylandSurface *surface, int hotspotX, int hotspotY);

signals:
    void startMove();
    void startResize(int edge, bool anchored);
    void dragStarted(dtkWMView *dragIcon);
    void frameOffset(const QPoint &offset);

public slots:
    void triggerRender();

private slots:
    void surfaceHasContentChanged();
    void surfaceDestroyed();
    void viewSurfaceDestroyed();
    void onStartMove();
    void onWlStartResize(QWaylandSeat *seat, QWaylandWlShellSurface::ResizeEdge edges);
    void onXdgStartResize(QWaylandSeat *seat, QWaylandXdgSurfaceV5::ResizeEdge edges);

    void startDrag();

    void onSurfaceCreated(QWaylandSurface *surface);
    void onWlShellSurfaceCreated(QWaylandWlShellSurface *wlShellSurface);
    void onXdgSurfaceCreated(QWaylandXdgSurfaceV5 *xdgSurface);
    void onXdgPopupRequested(QWaylandSurface *surface, QWaylandSurface *parent, QWaylandSeat *seat,
                             const QPoint &position, const QWaylandResource &resource);
    void onSetTransient(QWaylandSurface *parentSurface, const QPoint &relativeToParent, bool inactive);
    void onSetPopup(QWaylandSeat *seat, QWaylandSurface *parent, const QPoint &relativeToParent);

    void onSubsurfaceChanged(QWaylandSurface *child, QWaylandSurface *parent);
    void onSubsurfacePositionChanged(const QPoint &position);

    void updateCursor();
    void viewAnimationDone();

private:
    dtkWMView *finddtkWMView(const QWaylandSurface *s) const;

    QWindow *m_window = nullptr;

    QList<dtkWMView*> m_views;

    QWaylandWlShell *m_wlShell = nullptr;
    QWaylandXdgShellV5 *m_xdgShell = nullptr;
    QWaylanddtkWMView m_cursordtkWMView;

    int m_cursorHotspotX;
    int m_cursorHotspotY;
};

//
// dtkWMCompositor.h ends here
