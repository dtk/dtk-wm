// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <QtCore>
#include <QtGui>

class dtkWMCompositor;
class dtkWMView;

class dtkWMWindow : public QOpenGLWindow
{
public:
    dtkWMWindow(void);

    void setCompositor(dtkWMCompositor *comp);

protected:
    void initializeGL(void) override;

    void paintGL(void) override;

    void mousePressEvent(QMouseEvent *) override;
    void mouseReleaseEvent(QMouseEvent *) override;
    void mouseMoveEvent(QMouseEvent *) override;

    void keyPressEvent(QKeyEvent *) override;
    void keyReleaseEvent(QKeyEvent *) override;

private slots:
    void startMove(void);
    void startResize(int edge, bool anchored);
    void startDrag(dtkWMView *dragIcon);

private:
    enum GrabState {
            NoGrab,
          MoveGrab,
        ResizeGrab,
          DragGrab
    };

private:
    dtkWMView *viewAt(const QPointF &);
    bool mouseGrab(void) const { return m_grabState != NoGrab ;}
    void drawBackground(void);
    void sendMouseEvent(QMouseEvent *e, dtkWMView *target);

private:
    static QPointF getAnchoredPosition(const QPointF &anchorPosition, int resizeEdge, const QSize &windowSize);
    static QPointF getAnchorPosition(const QPointF &position, int resizeEdge, const QSize &windowSize);

private:
    QOpenGLTextureBlitter m_textureBlitter;
    QSize m_backgroundImageSize;
    QOpenGLTexture *m_backgroundTexture = nullptr;
    dtkWMCompositor *m_compositor = nullptr;
    QPointer<dtkWMView> m_mouseView;
    GrabState m_grabState = NoGrab;
    QSize m_initialSize;
    int m_resizeEdge;
    bool m_resizeAnchored;
    QPointF m_resizeAnchorPosition;
    QPointF m_mouseOffset;
    QPointF m_initialMousePos;
    dtkWMView *m_dragIconView = nullptr;
};

//
// dtkWMWindow.h ends here
