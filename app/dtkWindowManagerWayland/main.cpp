// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include <QtGui>

#include "window.h"
#include "compositor.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    Window window;

    Compositor compositor(&window);

    window.setCompositor(&compositor);
    window.resize(800,600);
    window.show();

    return app.exec();
}

//
// main.cpp ends here
